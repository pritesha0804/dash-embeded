# Dash Embedded
### Embedding in a React application
This example demonstrates embedding a simple [Dash Enterprise template application](https://dash-demo.plotly.host/Docs/templates/sample-app) in a basic react application based off of [`create-react-app`](https://create-react-app.dev/docs/getting-started/). 
There are 2 folders in this directory: react-app and dash-app.

### Running Locally
This example requires python and node as well as React version > 16.2 (16.13 recommended).

1. Setup localhost mapping. This is required because Dash Embedded will not work out of the box with your default localhost configuration because Google Chrome will not allow CORS/XHR requests from localhost. Add the following line to your `/etc/hosts` file:
`127.0.0.1 dash.tests` 

##### Run the Dash Application
This dash application was taken from the [Dash Enterprise template documentation](https://dash-demo.plotly.host/Docs/templates/).
To make this application embeddable, we've added `plugins=[dash_embedded.Embeddable(origins=['*'])]` to the `dash.Dash` constructor.
> **Please Note:** `origins=['*']` opens your app to requests from any domain, so presents a security risk if your app is accessible from the public internet. You should specify the actual domains of the applications that are allowed to access this app. See the `flask_cors` package for more information.

1. Enter the dash-app folder: `cd dash-app`
2. `pip install -r requirements.txt` to install the python requirements to run the dash application. This includes the open source `dash` library as well as the proprietary packages `dash-design-kit` and `dash-embedded` which will be downloaded from the Dash Enterprise server.
3. `python app.py` will run the application locally. 
4. In a second command line window proceed with the instructions to run the react application.

##### Run the React Application
This react application was created with `npx create-react-app` then modified slightly for compatibility with Dash Embedded. The modifications include: the addition of the minified dash_app_component file with `/* eslint-disable */`, modification of the `App.js` file to include the `dash_app_component.min` import and the embedded app, and modification of the `index.js` file to remove `React.StrictMode`. 

1. Enter the react app folder: `cd react-app`
2. `npm install` note: this will create a node_modules folder 
3. `npm start` this will start the application on your localhost. You can visit the application at [http://dash.tests:3000](http://dash.tests:3000)

##### Run the Angular Application
This angular application was created with `ng new` then modified slightly for compatibility with Dash Embedded. The modifications include: the addition of the minified dash_app_component file with `/* eslint-disable */`, modification of the `app.component` file to include the `dash_app_component.min` import and the embedded app, and added bunch of `React` packages & dependencies
The implementation is based on [SO Question](https://stackoverflow.com/questions/54408694/connect-angular-application-with-reactjs-app/54408718#54408718)

1. Enter the angular app folder: `cd angular-app`
2. `npm install` note: this will create a node_modules folder 
3. `ng serve` this will start the application on your localhost. You can visit the application at [http://dash.tests:4200](http://dash.tests:4200)