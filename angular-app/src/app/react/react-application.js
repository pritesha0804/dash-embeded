import { DashApp } from './dash_app_component.min.js';
  
  class ReactApp extends React.Component {
    constructor(props) {
      super(props);
    }
  
    render() {
        return (<DashApp
            config={{
              url_base_pathname: 'http://dash.tests:8050'
            }}
          />);
    }
  }
  
  export class ReactApplication {
  
    static initialize(
      containerId,
      injector
    ) {
      ReactDOM.render(
        <ReactApp injector={injector}/>,
        document.getElementById(containerId)
      );
    }
  }