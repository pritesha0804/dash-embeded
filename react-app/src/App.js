import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { DashApp } from 'dash-embedded-component';

window.React = React;
window.ReactDOM = ReactDOM;
window.PropTypes = PropTypes;

function App() {
  return (<DashApp
    config={{
      url_base_pathname: 'http://dash.tests:8050'
    }}
  />);
}

export default App;
